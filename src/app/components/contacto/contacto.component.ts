import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
export class ContactoComponent implements OnInit {

  loading: boolean = false;
  formulario!: FormGroup;
  mensaje!: string
  mensajes: string = 'EL FORMULARIO SE HA ENVIADO EXITOSAMENTE';
  constructor(private fb: FormBuilder ) { 
    this.crearFormulario();
  }

  crearFormulario(): void{
    this.formulario = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
      email: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      mensaje: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(120)]]
    });
  }

  get nombreNoValido(){
    return this.formulario.get('nombre')?.invalid && this.formulario.get('nombre')?.touched;
  }

  get correoNoValido(){
    return this.formulario.get('email')?.invalid && this.formulario.get('email')?.touched
  }

  get mensajeNoValido(){
    return this.formulario.get('mensaje')?.invalid && this.formulario.get('mensaje')?.touched
  }

  ngOnInit(): void {
  }

  enviar(): void{
    
    this.loading = true;
    
    setTimeout(() => {
      this.loading=false;
      this.LimpiarFormulario()
    }, 3000);

  }

  LimpiarFormulario(){
    this.mensaje = this.mensajes;
    setTimeout(() => {
      this.mensaje = ''
    }, 3000);
    this.formulario.reset();
  }


  MensajeEnviado(){
    this.mensaje = this.mensajes
  }

}
