import { Component, OnInit } from '@angular/core';
import { TareaService } from "../../services/tarea.service";

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-tarea-form',
  templateUrl: './tarea-form.component.html',
  styleUrls: ['./tarea-form.component.css']
})
export class TareaFormComponent implements OnInit {
  formulario!: FormGroup;

  nombre: string;
  apellido: string;
  email: string;
  celular: string;
  fecha: string;
  hora: string;
  description: string;

  constructor(public taskService: TareaService,private fb: FormBuilder) {
    this.crearFormulario();
  }



  crearFormulario(): void{
    this.formulario = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(3),Validators.maxLength(15)]],
      apellido: ['', [Validators.required, Validators.minLength(3),Validators.maxLength(30)]],
      email: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      celular: ['', [Validators.required, Validators.pattern("[0-9]{8}")]],
      fecha: ['',Validators.required],
      hora: ['',Validators.required],
      description: ['', [Validators.required, Validators.minLength(2)]],
    });
  }


  get nombreNoValido(){
    return this.formulario.get('nombre')?.invalid && this.formulario.get('nombre')?.touched;
  }
  get apellidoNoValido(){
    return this.formulario.get('apellido')?.invalid && this.formulario.get('apellido')?.touched;
  }

  get emailNoValido(){
    return this.formulario.get('email')?.invalid && this.formulario.get('email')?.touched;
  }

  get celularNoValido(){
    return this.formulario.get('celular')?.invalid && this.formulario.get('celular')?.touched;
  }
  get mensajeNoValido(){
    return this.formulario.get('mensaje')?.invalid && this.formulario.get('mensaje')?.touched;
  }
  get fechaNoValido(){
    return this.formulario.get('fecha')?.invalid && this.formulario.get('fecha')?.touched;
  }
  get horaNoValido(){
    return this.formulario.get('hora')?.invalid && this.formulario.get('hora')?.touched;
  }
  get descriptionNoValido(){
    return this.formulario.get('description')?.invalid && this.formulario.get('description')?.touched;
  }



  ngOnInit(): void {
  }


  Addtarea() {
    this.taskService.addTarea({
      nombre: this.formulario.value.nombre,
      apellido: this.formulario.value.apellido,
      email: this.formulario.value.email,
      celular: this.formulario.value.celular,
      fecha: this.formulario.value.fecha,
      hora: this.formulario.value.hora,
      description: this.formulario.value.description,
      hide: true
    });

    this.formulario.reset();
  }

}
