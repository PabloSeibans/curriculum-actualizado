import { RouterModule, Routes } from "@angular/router";
import { ContactoComponent } from "./components/contacto/contacto.component";
import { ContenidoComponent } from "./components/contenido/contenido.component";
import { InicioComponent } from "./components/inicio/inicio.component";

const APP_ROUTES: Routes = [
    { path: '', component: InicioComponent },
    { path: 'curriculum', component: InicioComponent },
    { path: 'agenda', component: ContenidoComponent},
    { path: 'contacto', component: ContactoComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'curriculum' }
];
export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
